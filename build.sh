#!/usr/bin/env bash

set -e

dest=${PREFIX}/data/standard/bianca/

mkdir -p ${dest}
cp standard/bianca/* ${dest}
chmod 0755 ${dest}/*
